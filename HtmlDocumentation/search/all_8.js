var searchData=
[
  ['reject_26',['Reject',['../class_marble_i_t_1_1_simple_r_e_s_t_lib_1_1_promise.html#ad56031b9b56c5f88c4e8149463190bb9',1,'MarbleIT.SimpleRESTLib.Promise.Reject(Exception error)'],['../class_marble_i_t_1_1_simple_r_e_s_t_lib_1_1_promise.html#ad56031b9b56c5f88c4e8149463190bb9',1,'MarbleIT.SimpleRESTLib.Promise.Reject(Exception error)']]],
  ['resolve_27',['Resolve',['../class_marble_i_t_1_1_simple_r_e_s_t_lib_1_1_promise.html#a8b14833305785674b0192a8f959f4170',1,'MarbleIT.SimpleRESTLib.Promise.Resolve()'],['../class_marble_i_t_1_1_simple_r_e_s_t_lib_1_1_promise.html#a5f328b44adfaa0d447f1d9bbd4263919',1,'MarbleIT.SimpleRESTLib.Promise.Resolve(T payload)']]],
  ['response_28',['Response',['../struct_marble_i_t_1_1_simple_r_e_s_t_lib_1_1_response.html',1,'MarbleIT.SimpleRESTLib.Response&lt; T &gt;'],['../struct_marble_i_t_1_1_simple_r_e_s_t_lib_1_1_response.html#a10f7accacd55936dd88a55c86980ef51',1,'MarbleIT.SimpleRESTLib.Response.Response()']]],
  ['response_2ecs_29',['Response.cs',['../_response_8cs.html',1,'']]],
  ['restclient_30',['RESTClient',['../class_marble_i_t_1_1_simple_r_e_s_t_lib_1_1_r_e_s_t_client.html',1,'MarbleIT::SimpleRESTLib']]],
  ['restclient_2ecs_31',['RESTClient.cs',['../_r_e_s_t_client_8cs.html',1,'']]],
  ['restclientconfigurator_32',['RESTClientConfigurator',['../class_marble_i_t_1_1_simple_r_e_s_t_lib_1_1_r_e_s_t_client_configurator.html',1,'MarbleIT.SimpleRESTLib.RESTClientConfigurator'],['../class_marble_i_t_1_1_simple_r_e_s_t_lib_1_1_r_e_s_t_client_configurator.html#a091757a08f4131f46f9fe0de347abc20',1,'MarbleIT.SimpleRESTLib.RESTClientConfigurator.RESTClientConfigurator()']]],
  ['restclientconfigurator_2ecs_33',['RESTClientConfigurator.cs',['../_r_e_s_t_client_configurator_8cs.html',1,'']]],
  ['restendpoint_34',['RESTEndpoint',['../class_marble_i_t_1_1_simple_r_e_s_t_lib_1_1_r_e_s_t_endpoint.html',1,'MarbleIT.SimpleRESTLib.RESTEndpoint'],['../class_marble_i_t_1_1_simple_r_e_s_t_lib_1_1_r_e_s_t_endpoint.html#af3074eee188b5ec9a3a4afa7c54b7fd9',1,'MarbleIT.SimpleRESTLib.RESTEndpoint.RESTEndpoint()']]],
  ['restendpoint_2ecs_35',['RESTEndpoint.cs',['../_r_e_s_t_endpoint_8cs.html',1,'']]]
];

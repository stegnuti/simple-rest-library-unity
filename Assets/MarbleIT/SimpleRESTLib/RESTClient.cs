﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

namespace MarbleIT.SimpleRESTLib
{
    /// <summary>
    /// Simple promise-based http requests to an API.
    /// A RESTClient instance will inject the current authorization token into every request header.
    /// Derive from this class for each API you want to communicate with.
    /// </summary>
    public abstract class RESTClient : MonoBehaviour
    {
        /// <summary>
        /// Full path/URL to the API, including the port if applicable.
        /// Put the trailing slash here so that you don't have to write it at the beginning of each
        /// relative path when sending requests. So, 'https://www.blabla.com/' instead of 'https://www.blabla.com'
        /// </summary>
        public string serverUrl;
        [HideInInspector]
        public string authorizationToken;
        
        /// <summary>
        /// Sends a HTTP GET request to serverUrl + relativePath.
        /// </summary>
        /// <param name="relativePath">Relative path to the endpoint of interest, i.e 'users' or 'users/comments'.</param>
        /// <param name="prepend">String that will be inserted before the raw response json</param>
        /// <param name="append">String that will be inserted after the raw response json</param>
        /// <typeparam name="T">This should be a [System.Serializable] data transfer object that will be deserialized from the response json.</typeparam>
        /// <returns>(Promise) Data transfer object of type T deserialized from the response json.</returns>
        public Promise<Response<T>> Get<T>(string relativePath, string prepend = "", string append = "") where T : class
        {
            Promise<Response<T>> promise = new Promise<Response<T>>();
            
            UnityWebRequest request = UnityWebRequest.Get(serverUrl + relativePath);
            SendRequest(request, promise, prepend, append);
            
            return promise;
        }
        
        /// <summary>
        /// Sends a HTTP GET request to serverUrl + relativePath.
        /// </summary>
        /// <param name="relativePath">Relative path to the endpoint of interest, i.e 'users' or 'users/comments'.</param>
        /// <typeparam name="T">This should be a [System.Serializable] data transfer object that will be deserialized from the response json.</typeparam>
        /// <returns>(Promise) List of type T deserialized from the response json assuming that it is a raw list, i.e '[{T}, {T}, {T}]'.</returns>
        public Promise<Response<List<T>>> GetList<T>(string relativePath)
        {
            Promise<Response<JSONList<T>>> listPromise = new Promise<Response<JSONList<T>>>();
            
            UnityWebRequest request = UnityWebRequest.Get(serverUrl + relativePath);
            SendRequest(request, listPromise, "{ \"list\": ", " }");

            Promise<Response<List<T>>> promise = new Promise<Response<List<T>>>();

            listPromise.Then(result =>
            {
                promise.Resolve(new Response<List<T>>(result.body.list, result.statusCode));
            });
            
            return promise;
        }

        /// <summary>
        /// Sends a HTTP GET request to serverUrl + relativePath without deserializing the response json into a data transfer object, but just returning it as a string.
        /// </summary>
        /// <param name="relativePath">Relative path to the endpoint of interest, i.e 'users' or 'users/comments'.</param>
        /// <param name="prepend">String that will be inserted before the raw response json</param>
        /// <param name="append">String that will be inserted after the raw response json</param>
        /// <returns>(Promise) The response json as a string.</returns>
        public Promise<Response<string>> GetString(string relativePath, string prepend = "", string append = "")
        {
            Promise<Response<string>> promise = new Promise<Response<string>>();
            
            UnityWebRequest request = UnityWebRequest.Get(serverUrl + relativePath);
            SendRequestString(request, promise, prepend, append);
            
            return promise;
        }
        
        /// <summary>
        /// Sends a HTTP POST request to serverUrl + relativePath.
        /// </summary>
        /// <param name="relativePath">Relative path to the endpoint of interest, i.e 'users' or 'users/comments'.</param>
        /// <param name="requestBody">This should be a [System.Serializable] data transfer object that will be serialized into the request json.</param>
        /// <param name="prepend">String that will be inserted before the raw response json</param>
        /// <param name="append">String that will be inserted after the raw response json</param>
        /// <typeparam name="T">This should be a [System.Serializable] data transfer object that will be deserialized from the response json.</typeparam>
        /// <returns>(Promise) Data transfer object of type T deserialized from the response json.</returns>
        public Promise<Response<T>> Post<T>(string relativePath, T requestBody, string prepend = "", string append = "") where T : class
        {
            Promise<Response<T>> promise = new Promise<Response<T>>();
            
            UnityWebRequest request = UnityWebRequest.Post(serverUrl + relativePath, JsonConvert.SerializeObject(requestBody));
            SendRequest(request, promise, prepend, append);
            
            return promise;
        }

        /// <summary>
        /// Sends a HTTP POST request to serverUrl + relativePath.
        /// </summary>
        /// <param name="relativePath">Relative path to the endpoint of interest, i.e 'users' or 'users/comments'.</param>
        /// <param name="requestBody">This should be a [System.Serializable] data transfer object that will be serialized into the request json.</param>
        /// <typeparam name="T">This should be a [System.Serializable] data transfer object that will be deserialized from the response json.</typeparam>
        /// <returns>(Promise) List of type T deserialized from the response json assuming that it is a raw list, i.e '[{T}, {T}, {T}]'.</returns>
        public Promise<Response<List<T>>> PostList<T>(string relativePath, T requestBody)
        {
            Promise<Response<JSONList<T>>> listPromise = new Promise<Response<JSONList<T>>>();
            
            UnityWebRequest request = UnityWebRequest.Post(serverUrl + relativePath, JsonConvert.SerializeObject(requestBody));
            SendRequest(request, listPromise, "{ \"list\": ", " }");

            Promise<Response<List<T>>> promise = new Promise<Response<List<T>>>();

            listPromise.Then(result =>
            {
                promise.Resolve(new Response<List<T>>(result.body.list, result.statusCode));
            });
            
            return promise;
        }
        
        /// <summary>
        /// Sends a HTTP POST request to serverUrl + relativePath.
        /// </summary>
        /// <param name="relativePath">Relative path to the endpoint of interest, i.e 'users' or 'users/comments'.</param>
        /// <param name="requestBody">This should be an IEnumerable of type T that will be serialized into the request json.</param>
        /// <typeparam name="T">This should be a [System.Serializable] data transfer object that will be deserialized from the response json.</typeparam>
        /// <returns>(Promise) List of type T deserialized from the response json assuming that it is a raw list, i.e '[{T}, {T}, {T}]'.</returns>
        public Promise<Response<List<T>>> PostList<T>(string relativePath, IEnumerable<T> requestBody)
        {
            Promise<Response<JSONList<T>>> listPromise = new Promise<Response<JSONList<T>>>();
            
            StringBuilder jsonStringBuilder = new StringBuilder();
            jsonStringBuilder.Append("[ ");

            foreach (T instance in requestBody)
            {
                jsonStringBuilder.Append(JsonConvert.SerializeObject(instance));
                jsonStringBuilder.Append(", ");
            }

            // Remove last ', '
            jsonStringBuilder.Remove(jsonStringBuilder.Length - 2, 2);
            
            jsonStringBuilder.Append(" ]");
            
            UnityWebRequest request = UnityWebRequest.Post(serverUrl + relativePath, jsonStringBuilder.ToString());
            SendRequest(request, listPromise, "{ \"list\": ", " }");

            Promise<Response<List<T>>> promise = new Promise<Response<List<T>>>();

            listPromise.Then(result =>
            {
                promise.Resolve(new Response<List<T>>(result.body.list, result.statusCode));
            });
            
            return promise;
        }
        
        /// <summary>
        /// Sends a HTTP POST request to serverUrl + relativePath without deserializing the response json into a data transfer object, but just returning it as a string.
        /// </summary>
        /// <param name="relativePath">Relative path to the endpoint of interest, i.e 'users' or 'users/comments'.</param>
        /// <param name="requestBody">Request body expressed as a string.</param>
        /// <param name="prepend">String that will be inserted before the raw response json</param>
        /// <param name="append">String that will be inserted after the raw response json</param>
        /// <returns>(Promise) The response json as a string.</returns>
        public Promise<Response<string>> PostString(string relativePath, string requestBody, string prepend = "", string append = "")
        {
            Promise<Response<string>> promise = new Promise<Response<string>>();
            
            UnityWebRequest request = UnityWebRequest.Post(serverUrl + relativePath, requestBody);
            SendRequestString(request, promise, prepend, append);
            
            return promise;
        }
        
        /// <summary>
        /// Sends a HTTP PUT request to serverUrl + relativePath.
        /// </summary>
        /// <param name="relativePath">Relative path to the endpoint of interest, i.e 'users' or 'users/comments'.</param>
        /// <param name="requestBody">This should be a [System.Serializable] data transfer object that will be serialized into the request json.</param>
        /// <param name="prepend">String that will be inserted before the raw response json</param>
        /// <param name="append">String that will be inserted after the raw response json</param>
        /// <typeparam name="T">This should be a [System.Serializable] data transfer object that will be deserialized from the response json.</typeparam>
        /// <returns>(Promise) Data transfer object of type T deserialized from the response json.</returns>
        public Promise<Response<T>> Put<T>(string relativePath, T requestBody, string prepend = "", string append = "") where T : class
        {
            Promise<Response<T>> promise = new Promise<Response<T>>();
            
            UnityWebRequest request = UnityWebRequest.Put(serverUrl + relativePath, JsonConvert.SerializeObject(requestBody));
            SendRequest(request, promise, prepend, append);
            
            return promise;
        }
        
        /// <summary>
        /// Sends a HTTP PUT request to serverUrl + relativePath.
        /// </summary>
        /// <param name="relativePath">Relative path to the endpoint of interest, i.e 'users' or 'users/comments'.</param>
        /// <param name="requestBody">This should be a [System.Serializable] data transfer object that will be serialized into the request json.</param>
        /// <typeparam name="T">This should be a [System.Serializable] data transfer object that will be deserialized from the response json.</typeparam>
        /// <returns>(Promise) List of type T deserialized from the response json assuming that it is a raw list, i.e '[{T}, {T}, {T}]'.</returns>
        public Promise<Response<List<T>>> PutList<T>(string relativePath, T requestBody)
        {
            Promise<Response<JSONList<T>>> listPromise = new Promise<Response<JSONList<T>>>();
            
            UnityWebRequest request = UnityWebRequest.Put(serverUrl + relativePath, JsonConvert.SerializeObject(requestBody));
            SendRequest(request, listPromise, "{ \"list\": ", " }");

            Promise<Response<List<T>>> promise = new Promise<Response<List<T>>>();

            listPromise.Then(result =>
            {
                promise.Resolve(new Response<List<T>>(result.body.list, result.statusCode));
            });
            
            return promise;
        }
        
        /// <summary>
        /// Sends a HTTP PUT request to serverUrl + relativePath.
        /// </summary>
        /// <param name="relativePath">Relative path to the endpoint of interest, i.e 'users' or 'users/comments'.</param>
        /// <param name="requestBody">This should be an IEnumerable of type T that will be serialized into the request json.</param>
        /// <typeparam name="T">This should be a [System.Serializable] data transfer object that will be deserialized from the response json.</typeparam>
        /// <returns>(Promise) List of type T deserialized from the response json assuming that it is a raw list, i.e '[{T}, {T}, {T}]'.</returns>
        public Promise<Response<List<T>>> PutList<T>(string relativePath, IEnumerable<T> requestBody)
        {
            Promise<Response<JSONList<T>>> listPromise = new Promise<Response<JSONList<T>>>();
            
            StringBuilder jsonStringBuilder = new StringBuilder();
            jsonStringBuilder.Append("[ ");

            foreach (T instance in requestBody)
            {
                jsonStringBuilder.Append(JsonConvert.SerializeObject(instance));
                jsonStringBuilder.Append(", ");
            }

            // Remove last ', '
            jsonStringBuilder.Remove(jsonStringBuilder.Length - 2, 2);
            
            jsonStringBuilder.Append(" ]");
            
            UnityWebRequest request = UnityWebRequest.Put(serverUrl + relativePath, jsonStringBuilder.ToString());
            SendRequest(request, listPromise, "{ \"list\": ", " }");

            Promise<Response<List<T>>> promise = new Promise<Response<List<T>>>();

            listPromise.Then(result =>
            {
                promise.Resolve(new Response<List<T>>(result.body.list, result.statusCode));
            });
            
            return promise;
        }
        
        /// <summary>
        /// Sends a HTTP PUT request to serverUrl + relativePath without deserializing the response json into a data transfer object, but just returning it as a string.
        /// </summary>
        /// <param name="relativePath">Relative path to the endpoint of interest, i.e 'users' or 'users/comments'.</param>
        /// <param name="requestBody">Request body expressed as a string.</param>
        /// <param name="prepend">String that will be inserted before the raw response json</param>
        /// <param name="append">String that will be inserted after the raw response json</param>
        /// <returns>(Promise) The response json as a string.</returns>
        public Promise<Response<string>> PutString(string relativePath, string requestBody, string prepend = "", string append = "")
        {
            Promise<Response<string>> promise = new Promise<Response<string>>();
            
            UnityWebRequest request = UnityWebRequest.Put(serverUrl + relativePath, requestBody);
            SendRequestString(request, promise, prepend, append);
            
            return promise;
        }
        
        /// <summary>
        /// Sends a HTTP DELETE request to serverUrl + relativePath.
        /// </summary>
        /// <param name="relativePath">Relative path to the endpoint of interest, i.e 'users' or 'users/comments'.</param>
        /// <param name="prepend">String that will be inserted before the raw response json</param>
        /// <param name="append">String that will be inserted after the raw response json</param>
        /// <typeparam name="T">This should be a [System.Serializable] data transfer object that will be deserialized from the response json.</typeparam>
        /// <returns>(Promise) Data transfer object of type T deserialized from the response json.</returns>
        public Promise<Response<T>> Delete<T>(string relativePath, string prepend = "", string append = "") where T : class
        {
            Promise<Response<T>> promise = new Promise<Response<T>>();
            
            UnityWebRequest request = UnityWebRequest.Delete(serverUrl + relativePath);
            SendRequest(request, promise, prepend, append);
            
            return promise;
        }
        
        /// <summary>
        /// Sends a HTTP POST request to serverUrl + relativePath.
        /// </summary>
        /// <param name="relativePath">Relative path to the endpoint of interest, i.e 'users' or 'users/comments'.</param>
        /// <typeparam name="T">This should be a [System.Serializable] data transfer object that will be deserialized from the response json.</typeparam>
        /// <returns>(Promise) List of type T deserialized from the response json assuming that it is a raw list, i.e '[{T}, {T}, {T}]'.</returns>
        public Promise<Response<List<T>>> DeleteList<T>(string relativePath)
        {
            Promise<Response<JSONList<T>>> listPromise = new Promise<Response<JSONList<T>>>();
            
            UnityWebRequest request = UnityWebRequest.Delete(serverUrl + relativePath);
            SendRequest(request, listPromise, "{ \"list\": ", " }");

            Promise<Response<List<T>>> promise = new Promise<Response<List<T>>>();

            listPromise.Then(result =>
            {
                promise.Resolve(new Response<List<T>>(result.body.list, result.statusCode));
            });
            
            return promise;
        }
        
        /// <summary>
        /// Sends a HTTP DELETE request to serverUrl + relativePath.
        /// </summary>
        /// <param name="relativePath">Relative path to the endpoint of interest, i.e 'users' or 'users/comments'.</param>
        /// <param name="prepend">String that will be inserted before the raw response json</param>
        /// <param name="append">String that will be inserted after the raw response json</param>
        /// <returns>(Promise) The response json as a string.</returns>
        public Promise<Response<string>> DeleteString(string relativePath, string prepend = "", string append = "")
        {
            Promise<Response<string>> promise = new Promise<Response<string>>();
            
            UnityWebRequest request = UnityWebRequest.Delete(serverUrl + relativePath);
            SendRequestString(request, promise, prepend, append);
            
            return promise;
        }
        
        private IEnumerator _SendWebRequest(UnityWebRequest request, Action callback)
        {
            yield return request.SendWebRequest();
            callback?.Invoke();
        }
        
        private void SendRequest<T>(UnityWebRequest request, Promise<Response<T>> promise, string prepend, string append) where T: class
        {
            Debug.Log($"RESTClient __REQUEST__ {request.method} {request.url}");
            request.SetRequestHeader("Authorization", $"Bearer {authorizationToken}");
            
            StartCoroutine(_SendWebRequest(request, () =>
            {
                string response = "";
                if (request.downloadHandler?.text != null)
                {
                    response = request.downloadHandler.text;
                }
                
                if (request.isHttpError || request.isNetworkError)
                {
                    Debug.Log($"RESTClient __RESPONSE__ {request.responseCode} {request.url}, error: {request.error}, response: {response}");
                    promise.Reject(new Exception(request.error));
                }
                else
                {
                    Debug.Log($"RESTClient __RESPONSE__ {request.responseCode} {request.url} {response}");

                    T responseDto;
                    
                    try
                    {
                        responseDto = JsonConvert.DeserializeObject<T>(prepend + response + append);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("JSON deserialization failed");
                    }
                    
                    promise.Resolve(new Response<T>(responseDto, request.responseCode));
                }
            }));
        }
        
        private void SendRequestString(UnityWebRequest request, Promise<Response<string>> promise, string prepend, string append)
        {
            
            Debug.Log($"RESTClient __REQUEST__ {request.method} {request.url}");
            request.SetRequestHeader("Authorization", $"Bearer {authorizationToken}");
            
            StartCoroutine(_SendWebRequest(request, () =>
            {
                string response = "";
                if (request.downloadHandler.text != null)
                {
                    response = request.downloadHandler.text;
                }
                
                if (request.isHttpError || request.isNetworkError)
                {
                    Debug.Log($"RESTClient __RESPONSE__ {request.responseCode} {request.url}, error: {request.error}, response: {response}");
                    promise.Reject(new Exception(request.error));
                }
                else
                {
                    Debug.Log($"RESTClient __RESPONSE__ {request.responseCode} {request.url} {response}");
                    promise.Resolve(new Response<string>(prepend + response + append, request.responseCode));
                }
            }));
        }

        [Serializable]
        private class JSONList<T>
        {
            public List<T> list;
        }
    }

}
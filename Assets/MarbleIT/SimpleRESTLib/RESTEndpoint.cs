using System;
using System.Collections.Generic;

namespace MarbleIT.SimpleRESTLib
{
    /// <summary>
    /// Defines one endpoint for the REST API a concrete RESTClient communicates with.
    /// Recommended use: make a public static reference to each endpoint in your concrete RESTClient class.
    /// Then, make requests to that endpoint by using the public methods in this class.
    /// </summary>
    public class RESTEndpoint
    {
        private readonly string _relativePath;
        private readonly RESTClient _restClient;
        
        /// <summary>
        /// </summary>
        /// <param name="relativePath">Path to the endpoint ignoring the server URL, i.e 'users/'</param>
        /// <param name="restClient">RESTClient instance that this endpoint will use</param>
        public RESTEndpoint(string relativePath, RESTClient restClient)
        {
            if (restClient == null)
            {
                throw new Exception("RESTClient reference cannot be null!");
            }
            _relativePath = relativePath;
            _restClient = restClient;
        }

        /// <summary>
        /// Sends a GET request to the endpoint path with the query parameters string concatenated.
        /// </summary>
        /// <param name="queryParameters"></param>
        /// <param name="prepend">String to insert before raw response json</param>
        /// <param name="append">String to insert after raw response json</param>
        /// <typeparam name="T"></typeparam>
        /// <returns>(Promise) Response parsed as type T</returns>
        public Promise<Response<T>> Get<T>(string queryParameters = "", string prepend = "", string append = "") where T : class
        {
            return _restClient.Get<T>(_relativePath + queryParameters, prepend, append);
        }
        
        /// <summary>
        /// Sends a GET request to the endpoint path with the query parameters string concatenated.
        /// </summary>
        /// <param name="queryParameters"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns>(Promise) List of type T pared from the response string'</returns>
        public Promise<Response<List<T>>> GetList<T>(string queryParameters = "")
        {
            return _restClient.GetList<T>(_relativePath + queryParameters);
        }

        /// <summary>
        /// Sends a GET request to the endpoint path with the query parameters string concatenated.
        /// </summary>
        /// <param name="queryParameters"></param>
        /// <param name="prepend">String to insert before raw response json</param>
        /// <param name="append">String to insert after raw response json</param>
        /// <returns>(Promise) Raw response string</returns>
        public Promise<Response<string>> GetString(string queryParameters = "", string prepend = "", string append = "")
        {
            return _restClient.GetString(_relativePath + queryParameters, prepend, append);
        }

        /// <summary>
        /// Sends a POST request to the endpoint path with the query parameters string concatenated.
        /// </summary>
        /// <param name="requestBody">System.Serializable data transfer object to serialize into JSON request body.</param>
        /// <param name="queryParameters"></param>
        /// <param name="prepend">String to insert before raw response json</param>
        /// <param name="append">String to insert after raw response json</param>
        /// <typeparam name="T"></typeparam>
        /// <returns>(Promise) Response parsed as type T</returns>
        public Promise<Response<T>> Post<T>(T requestBody, string queryParameters = "", string prepend = "", string append = "") where T : class
        {
            return _restClient.Post<T>(_relativePath + queryParameters, requestBody, prepend, append);
        }

        /// <summary>
        /// Sends a POST request to the endpoint path with the query parameters string concatenated.
        /// </summary>
        /// <param name="requestBody">JSON string to put into the request body</param>
        /// <param name="queryParameters"></param>
        /// <param name="prepend">String to insert before raw response json</param>
        /// <param name="append">String to insert after raw response json</param>
        /// <returns>(Promise) Raw response string</returns>
        public Promise<Response<string>> PostString(string requestBody, string queryParameters = "", string prepend = "", string append = "")
        {
            return _restClient.PostString(_relativePath + queryParameters, requestBody, prepend, append);
        }
        
        /// <summary>
        /// Sends a POST request to the endpoint path with the query parameters string concatenated.
        /// </summary>
        /// <param name="requestBody">System.Serializable data transfer object to serialize into JSON request body.</param>
        /// <param name="queryParameters"></param>
        /// <returns>(Promise) List of type T pared from the response string</returns>
        public Promise<Response<List<T>>> PostList<T>(T requestBody, string queryParameters = "")
        {
            return _restClient.PostList<T>(_relativePath + queryParameters, requestBody);
        }

        /// <summary>
        /// Sends a PUT request to the endpoint path with the query parameters string concatenated.
        /// </summary>
        /// <param name="requestBody">System.Serializable data transfer object to serialize into JSON request body.</param>
        /// <param name="queryParameters"></param>
        /// <param name="prepend">String to insert before raw response json</param>
        /// <param name="append">String to insert after raw response json</param>
        /// <typeparam name="T"></typeparam>
        /// <returns>(Promise) Response parsed as type T</returns>
        public Promise<Response<T>> Put<T>(T requestBody, string queryParameters = "", string prepend = "", string append = "") where T : class
        {
            return _restClient.Put<T>(_relativePath + queryParameters, requestBody, prepend, append);
        }

        /// <summary>
        /// Sends a PUT request to the endpoint path with the query parameters string concatenated.
        /// </summary>
        /// <param name="requestBody">JSON string to put into the request body</param>
        /// <param name="queryParameters"></param>
        /// <param name="prepend">String to insert before raw response json</param>
        /// <param name="append">String to insert after raw response json</param>
        /// <returns>(Promise) Raw response string</returns>
        public Promise<Response<string>> PutString(string requestBody, string queryParameters = "", string prepend = "", string append = "")
        {
            return _restClient.PutString(_relativePath + queryParameters, requestBody, prepend, append);
        }
        
        /// <summary>
        /// Sends a PUT request to the endpoint path with the query parameters string concatenated.
        /// </summary>
        /// <param name="requestBody">System.Serializable data transfer object to serialize into JSON request body.</param>
        /// <param name="queryParameters"></param>
        /// <returns>(Promise) List of type T pared from the response string</returns>
        public Promise<Response<List<T>>> PutList<T>(T requestBody, string queryParameters = "")
        {
            return _restClient.PutList<T>(_relativePath + queryParameters, requestBody);
        }

        /// <summary>
        /// Sends a DELETE request to the endpoint path with the query parameters string concatenated.
        /// </summary>
        /// <param name="queryParameters"></param>
        /// <param name="prepend">String to insert before raw response json</param>
        /// <param name="append">String to insert after raw response json</param>
        /// <typeparam name="T"></typeparam>
        /// <returns>(Promise) Response parsed as type T</returns>
        public Promise<Response<T>> Delete<T>(string queryParameters = "", string prepend = "", string append = "") where T : class
        {
            return _restClient.Delete<T>(_relativePath + queryParameters, prepend, append);
        }

        /// <summary>
        /// Sends a DELETE request to the endpoint path with the query parameters string concatenated.
        /// </summary>
        /// <param name="queryParameters"></param>
        /// <param name="prepend">String to insert before raw response json</param>
        /// <param name="append">String to insert after raw response json</param>
        /// <returns>(Promise) Raw response string</returns>
        public Promise<Response<string>> DeleteString(string queryParameters = "", string prepend = "", string append = "")
        {
            return _restClient.DeleteString(_relativePath + queryParameters, prepend, append);
        }
        
        /// <summary>
        /// Sends a DELETE request to the endpoint path with the query parameters string concatenated.
        /// </summary>
        /// <param name="queryParameters"></param>
        /// <returns>(Promise) List of type T pared from the response string</returns>
        public Promise<Response<List<T>>> DeleteList<T>(string queryParameters = "")
        {
            return _restClient.DeleteList<T>(_relativePath + queryParameters);
        }
    }
}